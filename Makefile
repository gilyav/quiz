CPPFLAGS += -std=c++11 -Wall

default: test

test: quiz
	./quiz tests/test1 tests/test1.out
	./quiz tests/test2 tests/test2.out
