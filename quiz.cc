#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>

using namespace std;

void usage(const char* name, string err="")
{
    if(err.length())
        cout << "Error: " << err << endl;
    cout << "Usage: " << name << " <infile> <outfile>" << endl;
}

string simplify(string str);
string sum(string x1, string x2);
string difference(string x1, string x2);
string product(string x1, string x2);
string quotient(string x1, string x2);

struct QuizException
{
    string val;
    QuizException(string str): val(str) {}
};

int main(int argc, char **argv)
{
    if(argc < 3) {
        usage(argv[0]);
        return -1;
    }

    ifstream ifs(argv[1]);
    if( !ifs.is_open() ) {
        usage(argv[0], "Can't open input file " + string(argv[1]));
        return -1;
    }

    string line;
    if(!getline(ifs, line)) {
        usage(argv[0], "Can't read line from input file " + string(argv[1]));
        return -1;
    }

    ifs.close();

    vector<string> operands;
    boost::split(operands, line, boost::is_any_of(" "), boost::token_compress_on);
    if(operands.size() != 2) {
        usage(argv[0], "Wrong operands number in input file " + string(argv[1]));
        return -1;
    }

    ofstream ofs;
    ofs.open(argv[2]);
    if(!ofs.is_open()) {
        usage(argv[0], "Can't write output file " + string(argv[2]));
        return -1;
    }
    try {
        string op1 = simplify(operands[0]);
        string op2 = simplify(operands[1]);

        ofs << op1 << " + " << op2 << " = " << simplify(sum(operands[0], operands[1])) << endl;
        ofs << op1 << " - " << op2 << " = " << simplify(difference(operands[0], operands[1])) << endl;
        ofs << op1 << " * " << op2 << " = " << simplify(product(operands[0], operands[1])) << endl;
        ofs << op1 << " / " << op2 << " = " << simplify(quotient(operands[0], operands[1])) << endl;
    }
    catch (QuizException &ex) {
        cout << "Got Exception while calculating line: \"" << line << "\" ("<< ex.val << ")" << endl;
    }
}

long gcd ( long a, long b )
{
    int c;
    while ( a != 0 ) {
        c = a; a = b%a;  b = c;
    }
    return b;
}

long lcm(long a, long b)
{
    long temp = gcd(a, b);

    return temp ? (a / temp * b) : 0;
}

void split(string str, long numbers[2])
{
    vector<string> nums;
    boost::split(nums, str, boost::is_any_of("/"));
    if(nums.size() != 2)
        throw QuizException("Error processing number \""+str+"\"");

    try {
        numbers[0] = stol(nums[0]);
        numbers[1] = stol(nums[1]);
    } 
    catch(...) {
        throw QuizException("Error processing number \""+str+"\"");
    }
}

string simplify(string str)
{
    long nums[2];

    if(str == "Inf")
        return str;
    split(str, nums);

    ostringstream os;
    bool neg = (nums[0] > 0 && nums[1] < 0) || (nums[0] < 0 && nums[1] > 0);
    if(neg)
        os << "(-";
    nums[0] = labs(nums[0]);
    nums[1] = labs(nums[1]);

    if(nums[0]) {
        long div = gcd(nums[0], nums[1]);
        nums[0] /= div;
        nums[1] /= div;
    }

    if(nums[0] >= nums[1]) {
        os << nums[0]/nums[1];
        if(nums[0]%nums[1])
            os << " " << nums[0]%nums[1] << "/" << nums[1];
    }
    else
        os << nums[0] << "/" << nums[1];

    if(neg)
        os << ")";
   
    return os.str();
}
string sum(string x1, string x2)
{
    long nums1[2];
    long nums2[2];
    split(x1, nums1);
    split(x2, nums2);
    long mul = lcm(nums1[1], nums2[1]);
    cout << nums1[1] << " " << nums2[1] << " " <<mul << endl;
    ostringstream os;
    os << mul/nums1[1]*nums1[0] + mul/nums2[1]*nums2[0] << "/" << mul;
    return os.str();
}

string difference(string x1, string x2)
{
    long nums1[2];
    long nums2[2];
    split(x1, nums1);
    split(x2, nums2);
    long mul = lcm(nums1[1], nums2[1]);
    ostringstream os;
    os << mul/nums1[1]*nums1[0] - mul/nums2[1]*nums2[0] << "/" << mul;
    return os.str();
}

string product(string x1, string x2)
{
    long nums1[2];
    long nums2[2];
    split(x1, nums1);
    split(x2, nums2);
    ostringstream os;
    os << nums1[0]*nums2[0] << "/" << nums1[1]*nums2[1];
    return os.str();
}

string quotient(string x1, string x2)
{
    long nums1[2];
    long nums2[2];
    split(x1, nums1);
    split(x2, nums2);
    if(nums2[0] == 0)
        return "Inf";
    ostringstream os;
    os << nums1[0]*nums2[1] << "/" << nums1[1]*nums2[0];
    return os.str();
}

